# Haptic Box

A box built by its listener which makes tangible sound affected by how it’s touched. See [https://daveriedstra.com/haptic-box](https://daveriedstra.com/works/haptic-box) for more info.

![Internal image of the box](http://daveriedstra.com/img/haptic-box-open-20220204-800.jpg)

This repository contains [build documents](build) and [code](code) for a haptic box. See the readme files in each of those directories as well as their respective pages on the website linked above for more info.
