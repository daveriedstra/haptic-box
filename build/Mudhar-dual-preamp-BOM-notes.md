# Notes on Mudhar dual low-noise preamp BOM

- My design did not duplicate the 47uf filter capacitor (connecting power pos to neg); restoring it by adding another in parallel might improve noise performance
- Terminals are the most expensive part of this BOM; cost can be brought down a few dollars by choosing a terminal with more connections and changing the layout accordingly
- Most of these components can be probably found at a local electronics supply shop (with the likely exception of the NE5534P op amp)
- Most components can be replaced with something that matches their spec. Important parts of the spec are resistance / capacitance, voltage rating (16V), and tolerance (5% for resistors, 20% for capacitors).
- Use a standard 0.1 inch / 2.54mm spaced stripboard.

## Parts

603-CFR-25JR-521M 2
603-CFR-25JR-5256K 4
603-CFR-25JR-521K 2
603-CFR25SJT-52-10K 2
603-CFR-25JR-52100K 2
603-CFR-25JB-52-470R 2
80-ESK106M016AC3KA 4 (cap 10uf)
647-UKT1C470MDD1TA 3 (cap 47uf)
595-NE5534P 2
512-1N4148TA 4 (diodes)
651-1725656 5 (terminals, dual)
