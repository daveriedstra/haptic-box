# Haptic Box - Build

This directory contains a collection of documents which describe my haptic box build. Read more about the usage and history of the build [here](https://daveriedstra.com/works/haptic-box/build.html).

- `box-diagrams.svg` and `box-cut-list.md` -- box design and cut list
- `electronics-layout.*` -- Diagram of internal electronics layout
- `Mudhar-dual-preamp-*` Preamp BOM, notes, and image
    - _N.B. This preamp is Richard Mudhar's design, available [here](https://www.richardmudhar.com/blog/piezo-contact-microphone-hi-z-amplifier-low-noise-version/), and the files included here are meant as documentation and build aids._
