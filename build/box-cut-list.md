# Box Cutlist

Box outer dimensions: 22.6cm H × 12.6cm W × 51cm L

Cut list:

- **Panels** 2 @ 51cm × 21cm × 0.6cm thick
- **Spine sides:** 2 @ 22.6cm × 11.3cm × 0.8cm thick
- **Spine top:** 1 @ 51cm × 11.3cm × 0.8cm thick
