# Haptic Box - Code

This directory is the home of the code that makes the audio process run, putting the "hap" in "haptic." Read more about the usage and history of this code [here](https://daveriedstra.com/works/haptic-box/code.html). Or, if you want to get up and running quickly, running this on the Raspberry Pi should get you started:

```
git clone https://gitlab.com/daveriedstra/haptic-box
cd haptic-box/code
./install.sh
```

- `haptic-box.sc` - the SuperCollider patch defining the audio process
- `button-handler.py` - a script that interprets button presses and starts / stops the patch or shuts down the Pi as appropriate
- `button-handler.service` - a systemd config file which starts `button-handler.py` when the Pi is booted
- `install.sh` and `uninstall.sh` - handles install / uninstall and configuration of the above files
