#!/usr/bin/env python3

"""
Button handler for the haptic box.
Started on boot by ________.service

Single press: start/stop haptic sclang patch
Long press: shut down Pi

Drawing on stuff from this how-to article:
https://howchoo.com/g/mwnlytk3zmm/how-to-add-a-power-button-to-your-raspberry-pi

RPi GPIO reference:
https://sourceforge.net/p/raspberry-gpio-python/wiki/Inputs/
"""

import RPi.GPIO as GPIO
import subprocess
import shutil
from pathlib import Path
from time import time, sleep
import asyncio

# File name of the patch to run
PATCH = 'haptic-box.sc'

# Which GPIO pin the button is wired to
# reference https://pinout.xyz
BTN_PIN = 15

# How long the button must be held to count as a long press
LONG_PRESS_DUR = 2

# How long the button must be held to count as a press at all
BOUNCE_DUR = 0.5

# full path to sclang binary, set in main()
sclang = ''

# Used in button debounce section
btn_held = False
first_falling_time = 0
last_falling_time = 0

# Whether the SuperCollider patch is running
patch_is_running = False


def shutdown():
    '''Shuts down the Pi'''

    print('Shutting down...')
    try:
        subprocess.run(['sudo', 'shutdown', 'now'])
    except subprocess.CalledProcessError as e:
        print('Couldn\'t shut down!')
        print(e)


def start_patch():
    '''Start the supercollider patch'''

    patch = (Path(__file__).parent / PATCH).resolve()
    if not patch.is_file():
        print(f'{patch} does not seem to be a file')
        return

    print(f'Starting the patch "{patch}"...')
    try:
        # subprocess.run(['sclang', PATCH], check=True)
        subprocess.Popen([sclang, patch],
                         stdout=subprocess.DEVNULL,
                         stderr=subprocess.DEVNULL)
    except Exception as e:
        print(f'Starting the patch at "{PATCH}" failed!')
        print(e)
        return

    print('Patch started.')


def stop_patch():
    '''Stop the supercollider patch'''

    print('Stopping the patch...')

    try:
        subprocess.run(['killall', 'scsynth'], check=True)
    except Exception as e:
        print(e)

    try:
        subprocess.run(['killall', 'sclang'], check=True)
    except Exception as e:
        print(e)


def toggle_patch():
    '''Toggle the SuperCollider patch.

    If the patch is running, stop it; otherwise, start it.
    '''

    if 0 == subprocess.call(['pgrep', 'sclang'], stdout=subprocess.DEVNULL):
        stop_patch()
    else:
        start_patch()


'''
Switch debounce
This is all complicated and can apparently be avoided with a 0.1uF cap between the button leads,
However, I don't know where to get one locally and I don't want to put together the minimum Mouser order.

Tack:

- A press is registered if there were no new FALLING events within bounce_dur of the last RISING evt
    - ie, continual switch bouncing is interpreted as if the switch is held down
- The duration that differentiates between a short and long press is from the FIRST FALLING event
    and the register
'''


async def on_falling():
    '''
    - Sets first_falling_time if not held
    - Sets btn_held = true
    - Sets last_falling_time always
    '''

    global btn_held
    global first_falling_time
    global last_falling_time

    now = time()

    if not btn_held:
        first_falling_time = now

    btn_held = True
    last_falling_time = now


async def on_rising():
    '''
    - Sleep for bounce_dur
    - If current_time - last_falling_time > bounce_dur, register press
    - If registered,
        - Set btn_held = False
        - is_short_press = (current_time - bounce_dur) - first_falling_time < long_press_dur
    '''

    global btn_held

    await asyncio.sleep(BOUNCE_DUR)
    current_time = time()
    is_press = (current_time - last_falling_time) > BOUNCE_DUR

    if not btn_held:
        return

    if is_press:
        # got a button press!
        btn_held = False

        # this should always be a short press because the "interrupt" method in main()
        # should unset btn_held (and also shut down the Pi), but it doesn't hurt to check.
        press_dur = current_time - BOUNCE_DUR - first_falling_time
        if press_dur < LONG_PRESS_DUR:
            toggle_patch()


async def main():
    global sclang
    sclang = shutil.which('sclang')

    # Set a pull UP resistor on the GPIO pin because the button connects it to ground
    # ie, pressing the button shorts out a normally present voltage
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(BTN_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    loop = asyncio.get_running_loop()

    # Just delegates the event to on_falling or on_rising
    def event_handler(ch):
        sleep(0.05)
        press = GPIO.input(ch) is 0
        asyncio.run_coroutine_threadsafe(on_falling() if press else on_rising(), loop)

    GPIO.add_event_detect(BTN_PIN, GPIO.BOTH, callback=event_handler, bouncetime=50)

    # This loop is primarily to keep the thread alive,
    # and it also checks for long button presses
    global btn_held
    while True:
        await asyncio.sleep(0.5) # arbitrary duration
        if btn_held and time() - first_falling_time > LONG_PRESS_DUR:
            # got a long press!
            # (short presses handled in on_rising)
            btn_held = False
            shutdown()


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print('cleaning up...')
        GPIO.cleanup(BTN_PIN)
