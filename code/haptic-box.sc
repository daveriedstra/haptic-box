/*
* Two channels of self-regulating bandpassed feedback.
*
* Each channel (left and right) is processed independently.
* Each channel's bands regulate their pitch movement and amplitude based on their amplitude envelope relative to the group.
*
* The bands move their centre pitch based on LFOs which share a base frequency and start in quadrature phase.
* The bands are continually analyzing their strength relative to the strength of the group.
* When a band's envelope is above `lfo_env_thresh` % of the amplitude range of the group, its LFO is slowed by `lfo_slow_ratio`.
* The band's amplitude is continually corrected in the direction of the mean amplitude of the group by multiplying it with the difference from the mean and a factor set by `self_correct`, to a maximum of 2.
* Because the LFOs will slow down when their bands pass over resonant nodes, their phase relations will shift and possibly even line up. The `lfo_phase_injection` variables are available to shift the LFOs back out of phase.
* The LFOs may be stopped with the `lfo_freeze` argument; phase injection and amplitude self-correction are not affected.
* The frequency range covered by the LFOs ranges from `freq` Hz on the low end to `spread` octaves above.
* The bands may additionally be moved into distinct sections of that range via the `splay` argument (when low, they overlap completely, when high, they occupy 1/4 of the range with no overlap).
*
* Additional behaviour (wet-dry, panning, etc) is simpler and explained inline.
*
* This version is a non-interactive script that auto-runs
*/

s.waitForBoot({
  ~register_synthdefs.value;
  s.sync;
  ~start_sound.value;
});

/**
* Resources
*/

// routing busses between pre, main, and post Synths
~routing_busses = 2.collect({ Bus.audio(s, 2) });

// Envelopes for 2 channels of 4 bands.
// They need to be available to the process "before" they're collected, so we need this buss.
~envs = Bus.control(s, 8);

// LFOs driving those same bands
// used for analysis to shape their phase later
~lfos = Bus.control(s, 8);

// Bus used by the ASR for its internal feedback
~asr_bus = Bus.control(s, 2);

/**
* Functions
*/

// Analog Shift Register with feedback.
~fdbk_asr = {
  arg input, clock, fdbk_coeff = 0, fdbk_bus;
  var sr = SampleDur.ir,
    fdbk_value = In.kr(fdbk_bus),
    new_val = (input * (1 - fdbk_coeff)) + (fdbk_value * fdbk_coeff),
    reg_0 = DelayN.kr(Latch.kr(new_val, clock), 0.1, sr),
    reg_1 = DelayN.kr(Latch.kr(reg_0, clock), 0.1, sr*2),
    reg_2 = DelayN.kr(Latch.kr(reg_1, clock), 0.1, sr*3),
    reg_3 = DelayN.kr(Latch.kr(reg_2, clock), 0.1, sr*4),
    reg = [reg_0, reg_1, reg_2, reg_3];

  // update feedback value for use in next calculation
  fdbk_value = 4.collect({|i| reg[i] * ((3 - i) / 1.5 - 1)}); // tilt over register values
  Out.kr(fdbk_bus, fdbk_value);
  reg
};

// Leading-edge rate limiting throttle:
// triggers at most once every period
// If trig will stay high and won't re-trigger the flipflop or the TDelay,
// have it gate an impulse train at half kr in order to reset
~throttle = {
  arg trig, period;
  var set = trig,
    reset = TDelay.kr(set, period);
  Trig.kr(SetResetFF.kr(set, reset), ControlDur.ir)
};

// determine the phase of a sine using arcsine and accounting for direction
// asc: (1 + asin).mod(1)
// desc: 0.5 - asin
~get_phase_kr = {
  arg input;
  var is_rising = input > Delay1.kr(input),
    asin = input.asin / (2 * pi);
  (is_rising * (1 + asin).mod(1)) + ((1-is_rising) * (0.5 - asin))
};

/**
* SynthDefs
*/

~register_synthdefs = {
  // PRE
  // clean up hum & apply spectral compression
  SynthDef(\pre, {
    arg in=0, out=0,
      pre_amp = 8; // gain before processing
    var sig = SoundIn.ar([in, in+1]);

    // Signal processing business:
    // apply pre-gain
    sig = sig * pre_amp;

    // filter out hum...
    sig = BRF.ar(sig, 50, 0.1);
    sig = BRF.ar(sig, 50, 0.1);
    sig = BRF.ar(sig, 50, 0.1);
    sig = BRF.ar(sig, 50, 0.1);

    // gentle spectral compression
    sig = FFT(LocalBuf(2.pow(9)!2), sig);
    sig = PV_Compander(sig, 50, 1, 0.8);
    sig = IFFT.ar(sig);

    Out.ar(out, sig);
  }).add;

  // MAIN
  // main filtering action
  SynthDef(\main, {
    arg in=0, out=0,
      env_busses,                 // the busses represented in ~envs
      lfo_busses,                 // the busses represented in ~lfos
      asr_bus,                   // the buss used by the ASR
      freq = 30,
      rq = 0.010245196850394,
      spread = 1.4,
      tilt = 0.4,                 // 0-1, 0.5 is balanced. Like a tilt filter if the bands were stacked low to high, but they very rarely will be.
      self_correct = 8,           // how much the bands will try to normalize their gain to the mean
      self_correct_time = 10,     // how long the selfcorrection is slewed (sec)
      splay = 0.66225963830948,   // how much are the bands spread across the range defined by spread? 0-1, like inverse overlap.
      lfo_freq = 0.0070928293280303, // frequency of the LFOs driving the band pitch mvt
      lfo_slow_ratio = 0.3,       // how much slower an LFO will be when the band env is above thresh
      lfo_freeze = 0,             // whether the band lfos are stopped
      lfo_env_thresh = 0.7,       // threshold of envelope range over which LFO is slowed
      asr_thresh_low = 0.2,
      asr_thresh_hi = 0.8,
      asr_thresh_period = 1,      // how long the env needs to be out of range to clock the ASR
      asr_throttle_period = 5;    // minimum duration between ASR clocks

    var sig = In.ar(in, 2),
      n_bands = 4,
      envs_in = [In.kr(env_busses, 4), In.kr(env_busses + 4, 4)],
      lfos_in = In.kr(lfo_busses, 8);

    //
    // signal business:
    //

    sig = sig.collect({ |chan, chan_idx|
      var chan_envs = envs_in[chan_idx],
        envs_max = chan_envs.reduce({|a,b| a.max(b)}),
        envs_min = chan_envs.reduce({|a,b| a.min(b)}),
        envs_mean = chan_envs.flat.mean,
        asr, asr_clock, asr_data,
        lfo_phase_injection=0;

      // ASR stuff:
      // when the mean of this channel's envelopes is continuously outside of the thresh / range for longer than thresh_period,
      // add a value to the ASR and get the register
      // then manipulate those values a bit and use as lfo_phase_injection

      // ASR clock triggers when continuously out of range for asr_thresh_period as most every asr_throttle_period
      asr_clock = InRange.kr(envs_mean, asr_thresh_low, asr_thresh_hi) < 1;
      asr_clock = (Sweep.kr(asr_clock) * asr_clock) > asr_thresh_period;
      asr_clock = ~throttle.value(asr_clock, asr_throttle_period);

      // ASR data is the range of the LFO phases.
      asr_data = 4.collect({|lfo_idx| ~get_phase_kr.value(lfos_in[lfo_idx + chan_idx])});
      asr_data = asr_data.reduce({|a,b| a.max(b)}) - asr_data.reduce({|a,b| a.min(b)});
      asr_data = ((1 - asr_data.abs) * 3).mod(3);

      asr = ~fdbk_asr.value(asr_data, asr_clock, fdbk_coeff: 0.2, fdbk_bus: asr_bus + chan_idx);
      lfo_phase_injection = asr[0];

      // generate bands
      chan = n_bands.collect({ |i|
        var band_sig,
          lfo, // lfo that drives pitch
          lfo_freq_mod, // modulator for lfo freq
          band_freq,
          band_gain, band_pos, band_env, relative_gain;

        // band_freq based on LFO which covers the set pitch range
        // and slows (but doesn't stop) when the envelope is high
        // Since these are quadrature phased, they start splayed
        // across the range as in the manual version

        lfo_freq_mod = (chan_envs[i] > (envs_max.lag(5) * lfo_env_thresh));
        lfo_freq_mod = lfo_freq_mod.linlin(0,1,1,lfo_slow_ratio).lag(2);
        lfo = SinOsc.kr(
          freq: lfo_freq * lfo_freq_mod * (lfo_freeze < 1),
          // the base of this phase expression is i / 4 * 2pi
          // it looks like I accidentally / 2 instead of * 2pi,
          // but it's actually / 4 * 2pi == / 2 * pi-- not an error!
          phase: ((i * lfo_phase_injection + i) / 2) * pi % 2pi
        );
        band_freq = lfo.linexp(-1, 1,
          (freq * 2.pow(spread * splay * i / 4)).lag(2),
          (freq * 2.pow(( 1 - (splay * (3-i) / 4)) * spread)).lag(2)
        );

        // get band signal
        band_sig = BPF.ar(chan, band_freq, rq);
        band_sig = BPF.ar(chan, band_freq, rq);
        band_env = EnvFollow.ar(band_sig, 0.99955);

        // store band envelope and LFO for later / earlier analysis
        Out.kr(env_busses + i + (4 * chan_idx), band_env);
        Out.kr(lfo_busses + i + (4 * chan_idx), lfo);

        // calculate band gain
        // first from tilt
        band_pos = (i / (n_bands - 1)).madd(2, (-1)); // normalize to -1 to 1
        band_gain = tilt * band_pos / 2 + 0.5; // apply tilt
        band_gain = band_gain.max(0).min(1); // clip to 0-1

        // then self-correct relative to other bands
        // max - min / env
        relative_gain = (band_env - envs_min) / (envs_max - envs_min);
        relative_gain = relative_gain.lag(self_correct_time);
        band_gain = band_gain + (1 - relative_gain * self_correct);

        band_sig * (band_gain.clip(0,2))
      });

      // mix down bands & send out
      Mix.ar(chan)
    });

    Out.ar(out, sig);
  }).add;

  // POST
  // pan, gain, fold, limit
  SynthDef(\post, {
    arg in = 0, out = 0,
      amp = 0.15,          // final additional gain
      fold_level = 0.18,  // output fold level
      mix = 1,            // 0-1 dry-wet
      pan = (-0.8),       // cross-panning: -1 hard L/R; 0 mono mix; 1 hard R/L
      lpf_freq = 300;

    var sig = In.ar(in, 2);

    // panning
    sig = Mix([
      Pan2.ar(sig[0], pan),
      Pan2.ar(sig[1], pan * -1)
    ]);

    // amp, fold, limit (TODO), low pass
    sig = (sig * amp).fold(fold_level * -1, fold_level);
    sig = LPF.ar(sig, lpf_freq);
    sig = LPF.ar(sig, lpf_freq);

    Out.ar(out, sig);
  }).add;
};

/**
* Initiate synths
*/
~start_sound = {
  ~pre = Synth.head(s, \pre, [
    \pre_amp, 8,
    \out, ~routing_busses[0]
  ]);
  ~main = Synth.tail(s, \main, [
    \in, ~routing_busses[0],
    \out, ~routing_busses[1],
    \env_busses, ~envs,
    \lfo_busses, ~lfos,
    \asr_bus, ~asr_bus,
    \lfo_freq, 0.2
  ]);
  ~post = Synth.tail(s, \post, [
    \amp, 0.6,
    \fold_level, 0.6,
    \lpf_freq, 100,
    \in, ~routing_busses[1]
  ]);
};

// ~pre.set(\pre_amp, 6);
// ~post.set(\amp, 0.08);
// ~post.set(\fold_level, 0.08);
// ~main.set(\freq, 37);
// ~main.set(\spread, 1.5);
