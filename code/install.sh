#! /usr/bin/env bash

# Safety first: https://jvns.ca/blog/2017/03/26/bash-quirks/
set -eu

# Installs the haptic box files and activates the systemd unit file

# Target locations
INSTALL_DIR="$HOME/.local/bin/haptic-box/"
UNIT_FILE_DIR="$HOME/.config/systemd/user/"

# Make sure the directories exist
mkdir -p $INSTALL_DIR
mkdir -p $UNIT_FILE_DIR

# Move the files to the relevant directories
cp button-handler.py $INSTALL_DIR
cp haptic-box.sc $INSTALL_DIR
cp button-handler.service $UNIT_FILE_DIR

# Tell the system to use the button handler
systemctl --user daemon-reload
systemctl --user enable button-handler.service
systemctl --user start button-handler.service

# Tell the user installation has finished
echo "Haptic Box code has been installed and enabled. Have a nice day."
