#! /usr/bin/env bash

# Deactivates the systemd unit file and uninstalls the haptic box files

INSTALL_DIR="$HOME/.local/bin/haptic-box/"
UNIT_FILE_DIR="$HOME/.config/systemd/user/"

systemctl --user stop button-handler.service
systemctl --user disable button-handler.service

rm "$INSTALL_DIR"button-handler.py
rm "$INSTALL_DIR"haptic-box.sc
rm "$UNIT_FILE_DIR"button-handler.service

systemctl --user daemon-reload

echo "Haptic Box code has been disabled and removed. Have a nice day."
